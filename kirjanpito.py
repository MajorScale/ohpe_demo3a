"""
Ohjelma ei tuotettu error-handlingia ajatellen. ;)
"""

import json, os


def load(): 
    path = input("Tiedoston nimi: ")
    with open(path, "r") as f:
        data = json.load(f)
    
    return data


def save(data):
    path = input("Tiedoston nimi: ")
    with open(path, "w") as f:
        f.write(json.dumps(data, indent=4))

    print("Tallennettu")        


def add_expense(data):
    name = input("Nimi: ")
    amount = float(input("Hinta: "))
    data["expenses"][name] = amount


def add_income(data):
    name = input("Nimi: ")
    amount = float(input("Hinta: "))
    data["incomes"][name] = amount


def expenses(data):
    return sum(data["expenses"].values()) or 0


def incomes(data):
    return sum(data["incomes"].values()) or 0


def print_expenses(data):
    for k, v in data["expenses"].items():
        print(f"{k} {v}")


def print_incomes(data):
    for k, v in data["incomes"].items():
        print(f"{k} {v}")


def print_summary(data):
    start = data["startBalance"]
    inc = incomes(data)
    exp = expenses(data)
    print(f"Start balance: {start}")
    print(f"Tulot: {inc}")
    print(f"Menot: {exp}")
    print(f"Balance after: {start - exp + inc}")


def print_commands():
    print("po: Poistu")
    print("tal: Tallenna")
    print("?: Komennot")
    print()
    print("lm: Lisää menoja")
    print("lt: Lisää tuloja")
    print("yht: Tulosta yhteenveto")
    print("et: Etsi")
    print("tult: Tulosta tulot")
    print("tulm: Tulosta menot")


def search(data):
    print("1: Meno nimellä")
    print("2: Menot rajalla")
    print("3: Tulo nimellä")
    print("4: Tulot rajalla")

    action = input("Mitä etsit: ")

    if action == "1":
        search_term = input("Tapahtuman nimi: ")
        print(data["expenses"][search_term])
    elif action == "2":
        amt = float(input("Raja: "))
        for name, amount in data["expenses"].items():
            if amount >= amt:
                print(f"{name} {amount}")
    elif action == "3":
        search_term = input("Tapahtuman nimi: ")
        print(data["incomes"][search_term])
    elif action == "4":
        amt = float(input("Raja: "))
        for name, amount in data["incomes"].items():
            if amount >= amt:
                print(f"{name} {amount}")


def main():
    print("1: Anna alkusaldo")
    print("2: Lataa tiedosto")

    data = {
        "expenses": {},
        "incomes": {},
    }

    inp = input()
    if inp == "1":
        start_balance = int(input("Syötä alkusaldo: "))
        data["startBalance"] = start_balance
    elif inp == "2":
        data = load()

    os.system("cls")

    print_commands()

    while True:
        action = input("> ")

        if action == "po":
            exit()
        elif action == "tal":
            save(data)
        elif action == "?":
            print_commands()
        elif action == "lm":
            add_expense(data)
        elif action == "lt":
            add_income(data)
        elif action == "yht":
            print_summary(data)
        elif action == "et":
            search(data)
        elif action == "tult":
            print_incomes(data)
        elif action == "tulm":
            print_expenses(data)
        

main()